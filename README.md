# Liquid Galaxy Docs

Conceitos básicos sobre bancos de dados relacionais:

{% content-ref url="bd/" %}
[bd](bd/)
{% endcontent-ref %}

CI/CD com GitLab e Heroku

{% content-ref url="ci-cd/" %}
[ci-cd](ci-cd/)
{% endcontent-ref %}

Vuex codelab

{% content-ref url="vuex/" %}
[vuex](vuex/)
{% endcontent-ref %}

Passo a passo de como adicionar um novo doc.

{% content-ref url="contribute/" %}
[contribute][contribute/]
{% endcontent-ref %}