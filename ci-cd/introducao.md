# Introdução

## O que é CI/CD ?

Integração e Desenvolvimento contínuo é um metódo que busca monitorar e automatizar os processos de teste, integração, entrega e implantação presentes no ciclo de vida de uma aplicação.

![Software Development Life Cycle Phases](https://i.pinimg.com/originals/79/64/86/796486b77cf46b8b10ccad8fc87f9206.jpg "SDLC")

O CI/CD se baseia nos conceitos de integração, entrega e implantação continua. Todos os conceitos do CI/CD, juntos, são conhecidos como `Pipeline de CI/CD` e representam toda uma cadeia de etapas realizadas até que um software seja disponibilizado para o cliente final.

![Fluxo de CI/CD](https://www.redhat.com/cms/managed-files/styles/wysiwyg_full_width/s3/ci-cd-flow-desktop_edited_0.png?itok=TzgJwj6p "Fluxo de CI/CD")

