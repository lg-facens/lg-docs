# Integração Contínua

## O que é a integração contínua ?

O desenvolvimento de um software geralmente é realizado por uma equipe de desenvolvedores onde, cada pessoa realiza uma tarefa e em um determinado momento todas essas tarefas são juntadas em uma só. O processo de revisão manual de um software pode se tornar algo cansativo pois, no processo de juntar todo o trabalho feito, podem ocorrer conflitos devido a muitas alterações em um mesmo arquivo.  
Com a Integração Continua, é possivel definir uma rotina de verificação do código, onde, todos os testes são executados de forma automática e caso não haja nenhum erro é gerada uma build da versão atual do software.

## Como funciona ?

A integração continua é realizada atráves de um software que monitora as alterações realizadas no código fonte da aplicação, que esta hospedado em algum serviço como github, gitlab, bitbuck e outros. Alguns exemplos de softwares que realizam a integração continua são: Jenkins, Travis CI, Github, Gitlab, etc. Os softwares de CI monitoram por alterações realizadas em uma branch especifica (por convenção é a branch master ou main) e a cada novo commit realizado dentro desta branch um job é iniciado fazendo a verificação do código. As verificações que seram executadas pelo software monitor são definidas em um arquivo `.yml` que contem comandos para serem executados antes de gerar a build da aplicação.