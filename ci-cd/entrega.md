# Entrega e Implantação Contínua

Logo após a integração contínua podemos definir no nosso pipeline de CI/CD para que se assuma o caminho da entrega ou implantação contínua.

![Fluxo CI/CD](https://semaphoreci.com/wp-content/uploads/assets/images/2017-07-27/cicd-flow.jpg "Fluxo CI/CD")

## Entrega Contínua

Na entrega contínua todo o código que foi previamente verificado no processo de integração é implantando em um ambiente de teste (staging) antes de ser implantando no ambiente de produção. A entrega contínua é um passo anterior a implantação e nos permite aplicar mais testes ao software (agora do ponto de vista de um usuário) antes de lançar para o publico. Após realizar testes manuais no ambiente de teste, apenas com um clique de um botão todo o código é implantando no ambiente de produção.

## Implantação Contínua

Na implantação contínua todo o código que foi previamente verificado no processo de integração é implantado diretamente no ambiente de produção, e o unico motivo para que um implantação seja cancelada é uma falha no processo de integração.