# Introdução

## O que é um banco de dados ?

Banco de dados é uma estrutura onde são armazenados informações de qualquer tipo.  
Hoje em dia ainda existem empresas que armazenam suas informações no papel guardados em ficharios, entretanto, a maioria das empresas adotaram o uso dos bancos de dados para esta tarefa, isto porque, com o banco de dados o acesso as informações se tornam mais fáceis.

## Porque relacional ?

O nome relacional refere-se ao modelo do banco de dados.
Ao longo dos anos modelos foram propostos para representação dos dados, dentre esse modelos temos o hierárquico, em rede e relacional.

### Modelo Hierárquico

No modelo hierárquico os dados são organizados em uma estrutura do típo árvore, isto é, todos os registros herdam de um unico registro "pai" ou raiz, formando uma hierarquia.

![Modelo Hierárquico](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/database/discovery/hierarchical-model.svg "Modelo Hierárquico")

### Modelo em Rede

No modelo em rede temos o mesmo conceito do modelo hierárquico, entretanto, neste caso existe a possibilidade de multiplos registros "pai".

![Modelo em Rede](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/database/discovery/network-model.svg "Modelo em Rede")

### Modelo Relacional

O modelo relacional é o modelo mais comum atualmente e classifica os dados em tabelas, onde, cada coluna lista um atributo, como nome, data de nascimento, endereço.  
No modelo relacional, como o proprio nome diz, existe a possibilidade de relacionar as tabelas. Em um modelo relacional uma tabela deve sempre conter uma coluna para representar toda a tabela, esta coluna é conhecida como chave primaria e a mesma nunca deve estar vazia e por convenção é sempre atribuido um valor numerico para a mesma. Uma chave referenciada em uma outra tabela é conhecida como chave estrangeira e a mesma representa o relacionamento entre duas tabelas.  

![Modelo Relacional](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/database/discovery/relational-model.svg "Modelo Relacional")

## Como são compostos os bancos relacionais ?

Um Banco de dados é composto por quatro partes, sendo essas: base de dados, sistema gerenciador de banco de dados (sgbd), linguagem de exploração e programas adicionais.

### Base de dados

A base de dados representa estrutura do banco de dados em si.

### Sistema gerenciador de banco de dados (SGBD)

São os sistemas que permitem o gerenciamento dos dados armazenados em um banco.  
Cada banco de dados possui o seu próprio SGBD como por exemplo o Workbench do banco MySQL ou então o SQL Server Management Studio do SQL Server, entretanto, existem SGBD's universais, que funcionam com qualquer banco, um exemplo seria o DBeaver, que possui uma versão (community) gratuita que se conecta com qualquer banco de dados relacional.

### Linguagem de exploração

Os bancos de dados possuem uma unica linguagem que permite o acesso aos dados.

### Programas adicionais

São todos aqueles programas utilizados para um fim especifico, como por exemplo, gerencia de usuarios, otimizadores de dados, etc.


