# Modelo Entidade Relacionamento

## O que é o Modelo Entidade Relacionamento ?

O modelo entidade relacionamento é um conceito que busca representar de maneira abstrata algo do "mundo real" descrevendo suas caracteristicas e relacionamentos.

## Quais os principais elementos em uma modelagem ER ?

Os principais elementos em uma modelagem entidade relacionamento de um banco são as entidades, seus atributos e os relacionamentos.

### Entidades

As entidades são os objetos do "mundo real". Por exemplo ao representar um hospital, as possiveis entidades podem ser "Medicos" e "Pacients".

### Relacionamentos

O relacionamento é a associação entre as entidades, sendo que cada entidade possui uma função dentro do relacionamento. Por exemplo, dentro do mesmo caso do hospital, supondo que haja as entidades "Medico", "Paciente", "Hospital" um possivel relacionamento é o de "Atendimento" que representa o ato de atendimento do paciente.

### Atributos

Os atributos são as caracteristicas de uma entidade, podendo tambem fazer parte dos relacionamentos. Por exemplo na entidade "livro" podem haver os atributos "nome do livro", "autor", "genero", entre outros.  

## Quais os tipos básicos de atributos ?

Atributos podem ser classificados de diversas maneiras, sendo essas: simples, composto, multivalorado e determinante.

### Atributo Simples

São atributos considerados atomicos ou indivisíveis, isto é, que não podem ser divididos em valores menores, um exemplo seria o CPF.

### Atributo Composto

Os atributos compostos podem ser divididos em partes menores com significados independentes, um exemplo seria o atributo "endereço" que pode ser dividido em "rua", "numero", "bairro", "cidade", entre outros.

### Atributo Multivalorado

Atributos multivalorados podem ter mais de um valor em uma mesma entidade, um exemplo seria um atributo "telefone", em um caso onde uma pessoa possa ter multiplos telefones.

### Atributo Determinante

São atributos que identificam de forma unica uma entidade, ou seja, não se repetem. Os atributos determinantes serão utilizado como chaves primarias no banco.