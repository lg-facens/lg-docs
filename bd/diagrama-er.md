# Diagrama Entidade Relacionamento

O diagrama Entidade Relacionamento é a representação gráfica do modelo ER. No Diagrama ER as entidades, seus atributos e relacionamentos são representados por figuras geometricas.

## Quais as formas em um Diagrama ER ?

As possiveis formas em um diagrama ER são, retângulo, losango e elipses.

### Entidades

As entidades são representadas por retângulos.

![Retângulo](https://dhg1h5j42swfq.cloudfront.net/2021/05/10191650/image-220.png "Retângulo")

### Relacionamentos

Os relacionamentos são representados por losangos.

![Losango](https://dhg1h5j42swfq.cloudfront.net/2021/05/10191713/image-221.png "Losango")

### Atributos

Os atributos são representados por elipses.

![Elipse](https://dhg1h5j42swfq.cloudfront.net/2021/05/10191726/image-222.png "Elipse")

### Atributos Multivalorados

Os atributos multivalorados são representados por.

![Multivalorados](https://dhg1h5j42swfq.cloudfront.net/2021/05/10191747/image-223.png "Multivalorados")

### Atributos Compostos

Os atributos compostos são representados por.

![Compostos](https://dhg1h5j42swfq.cloudfront.net/2021/05/10191810/image-224.png "Compostos")

Representando por meio de um diagrama as entidades "Medico" e "Paciente" com seus atributos e relacionamentos temos.

![Modelo ER](https://dhg1h5j42swfq.cloudfront.net/2021/05/10192407/image-226.png "Modelo ER")

## Cardinalidades

As cardinalidades são graus que definem a relação entre duas entidades. 

### Grau de relacionamento 1:1

Neste grau de relacionamento um elemento de uma entidade A se relaciona apenas com 1 e somente um elemento de uma entidade B.

### Grau de relacionamento 1:N

Neste grau de cardinalidade um elemento de uma entidade A pode se relacionar com multiplos elementos de uma entidade B. Neste caso a entidade que possui a cardinalidade máxima (N) deve conter o atributo chave estrangeira.

### Grau de relacionamento N:N

Neste grau de cardinalidade multiplos elementos de uma entidade A se relacionam com multiplos elementos de uma Entidade B. Neste tipo de cardinalidade uma nova tabela (tabela de junção) é gerada contendo as chaves estrangeiras de ambas entidades.

![Cardinalidade](https://dhg1h5j42swfq.cloudfront.net/2021/05/10192441/image-227.png "Cardinalidade")

No exemplo acima podemos observar as cardinalidades, onde, um professor ministra no minimo 0 e no maximo n (muitas) disciplinas e uma disciplina é ministrada por no minimo 1 e no máximo 1 professor, isto significa que, um professor pode ministrar nenhuma ou varias disciplinas e uma disciplina deve conter um e somente um professor.  
