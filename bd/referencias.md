# Referências

[Curso de Banco de Dados MySQL](https://www.youtube.com/playlist?list=PLHz_AreHm4dkBs-795Dsgvau_ekxg8g1r)  
[O que é um modelo de banco de dados?](https://www.lucidchart.com/pages/pt/o-que-e-um-modelo-de-banco-de-dados/#section_6)  
[Entenda o Modelo Entidade Relacionamento para o concurso da PF](https://www.estrategiaconcursos.com.br/blog/modelo-entidade-relacionamento/)  
[SQL: O que é e como usar os principais comandos básicos SQL](https://blog.betrybe.com/sql/)