# Structured Query Language

## O que é o SQL ?

Structured Query Language ou Linguagem de consulta Estruturada é uma linguagem de exploração, isto é, linguagem para acesso de informações em um banco de dados.  
O SQL é a linguagem utilizada pelos Sistemas Gerenciadores de Banco de Dados e permite o acesso e manipulação de informações em um banco de dados. Com o SQL é possível criar, inserir, atualizar, excluir e consultar as informações armazenadas na base.

## Quais os comandos básicos ?

O SQL possui cinco operações basicas (criar, inserir, atualizar, excluir e listar) que podem ser executadas atráves de comandos.

### SQL CREATE

O comando create pode ser utilizado tanto para criar um novo banco de dados quanto para criar uma nova tabela dentro de um banco.

```
CREATE DATABASE nome_do_banco;
```

```
CREATE TABLE nome_da_tabela (
    coluna1 tipo,
    coluna2 tipo
    ...
);
```

### SQL INSERT

Comando utilizado para inserir informações em uma tabela.

```
INSERT INTO nome_da_tabela (coluna1, coluna2, ...)
VALUES (valor1, valor2, ...);
```

Com este mesmo comando, pode-se suprimir as colunas quando se quer adicionar valores para todas as colunas da tabela, entretanto, neste caso os valores devem seguir a ordem em que as colunas foram criadas.

### SQL UPDATE

Comando utilizado para atualizar o valor de uma ou varias colunas em uma tabela.

```
UPDATE nome_da_tabela SET coluna1 = valor1, coluna2 = valor2, ...
WHERE condição;
```

### SQL DELETE

Comando utilizado para excluir uma entrada em uma tabela. `Lembre-se que a condição define o que vai ser excluido, isto implica, que um comando de exclusão sem condição irá remover todos os dados da tabela, seja cauteloso`.

```
DELETE FROM nome_da_tabela WHERE condição;
```

### SQL SELECT

Comando utilizado para listar registros da tabela.

```
SELECT coluna1, coluna2, ... FROM nome_da_tabela;
```

No comando de listagem, para listar todas as colunas de uma tabela pode-se utilizar o operador de indireção (*).

```
SELECT * FROM nome_da_tabela;
```

## Juntando tabelas

Como visto anteriormente os bancos relacionais permitem criar relações entre tabela utilizando chaves. Atráves do SQL é possível juntar os elementos de diferentes tabelas, desde que as tabelas possuam uma coluna em comum.  
Para juntar as colunas de duas ou mais tabelas utiliza-se o comando `JOIN`.

![SQL Joins](https://res.cloudinary.com/practicaldev/image/fetch/s--07Go4Ldi--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/i/u4rx9tnq7ei4fstlafec.png "SQL Joins")

### SQL INNER JOIN

Comando que junta duas tabelas e retorna os valores em comum.

```
SELECT colunas...
FROM nome_da_tabela1
INNER JOIN nome_da_tabela2
ON nome_da_tabela1.coluna = nome_da_tabela2.coluna;
```

### SQL LEFT JOIN

Comando que junta duas tabelas e retorna todos os valores da tabela a esquerda (tabela1) e os valores em comum da tabela a direita (tabela2).

```
SELECT colunas...
FROM tabela1
LEFT_JOIN tabela2
ON tabela1.coluna = tabela2.coluna;
```

### SQL RIGHT JOIN

Comando que junta duas tabelas e retorna todos os valores da tabela a direita (tabela2) e os valores em comum da tabela a esquerda (tabela1).

```
SELECT colunas...
FROM tabela1
RIGHT_JOIN tabela2
ON tabela1.coluna = tabela2.coluna;
```

### SQL FULL JOIN

Comando que junta duas tabelas e retorna todas as linha de ambas as tabelas, possuindo correspondentes ou não.

```
SELECT colunas...
FROM tabela1
FULL_JOIN tabela2
ON tabela1.coluna = tabela2.coluna;
```

