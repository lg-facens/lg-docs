# PokéAPI App

O código fonte deste codelab pode ser encontrado no [GitHub](https://github.com/hvini/pokeapi-vuex).  
Caso necessite, a solução encontra-se na branch `solution`, no mesmo repositório.

## Problemática

Quando iniciamos o app, podemos ver que o mesmo possui duas rotas: a página inicial, que contém uma toolbar com um link para a página inicial e um botão que nos leva para uma outra página, e abaixo da toolbar, uma lista com 9 pókemons, e temos támbem uma página de favoritos, que possui uma lista com 3 pókemons.  
Na página inicial conseguimos favoritar e desfavoritar um pokemon e quando olhamos no console do navegador vemos uma lista com os pokemons favoritos. Já na página de favoritos conseguimos remover um pókemon do favorito.  
A aplicação parece funcionar bem, entretanto, nota-se que não existe nenhuma integração entre as páginas, isto é, os pókemons adicionados como favoritos na página inicial não refletem na página de favoritos, indicando que os dados possivelmente são estáticos.

Por padrão, com o VueJs, podemos passar informações entre componentes utilizando props e emit.

![props-emit](https://user-images.githubusercontent.com/43768274/192028186-dbeffdb2-ca2e-496d-a6fc-0b6129766b62.png)

Apesar das props e emits funcionarem muito bem para casos menores, quando queremos construir algo mais complexo as props e emit acabam tornando o código complexo e por isso o ideal é que se utilize alguma ferramenta para o gerenciamento de estado da aplicação.

Com as stores do Vuex conseguimos centralizar as informações da nossa aplicação de forma que a mesma seja acessível globalmente, isto é, em qualquer componente.

## State

O state é o bloco onde definimos as váriaveis da aplicação. Na `Home` view temos uma lista de objetos com uma quantidade fixa de pokemons, vamos move-la para dentro do state na store.

```js
state: {
  pokemons: [
    {
      id: 1,
      name: 'bulbasaur',
      url: 'https://pokeapi.co/api/v2/pokemon/1/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/1.png'
    },
    {
      id: 2,
      name: 'ivysaur',
      url: 'https://pokeapi.co/api/v2/pokemon/2/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/2.png'
    },
    {
      id: 3,
      name: 'venusaur',
      url: 'https://pokeapi.co/api/v2/pokemon/3/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/3.png' 
    },
    {
      id: 4,
      name: 'charmander',
      url: 'https://pokeapi.co/api/v2/pokemon/4/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/4.png' 
    },
    {
      id: 5,
      name: 'charmeleon',
      url: 'https://pokeapi.co/api/v2/pokemon/5/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/5.png' 
    },
    {
      id: 6,
      name: 'charizard',
      url: 'https://pokeapi.co/api/v2/pokemon/6/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/6.png' 
    },
    {
      id: 7,
      name: 'squirtle',
      url: 'https://pokeapi.co/api/v2/pokemon/7/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/7.png' 
    },
    {
      id: 8,
      name: 'wartortle',
      url: 'https://pokeapi.co/api/v2/pokemon/8/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/8.png' 
    },
    {
      id: 9,
      name: 'blastoise',
      url: 'https://pokeapi.co/api/v2/pokemon/9/',
      imageUrl: 'https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/9.png' 
    }
  ]
}
```

Quando executamos novamente a aplicação vemos um erro, isto porque a view esta tentando acessar uma informação que não existe mais, por isso vamos pegar a nossa lista, que agora esta na store, dentro da Home View, utilizando o bloco `computed`

```js
computed: {
  pokemons() {
    return this.$store.state.pokemons
  }
}
```

agora, quando acessamos a aplicação podemos ver os dados sendo exibidos novamente, e agora vindo da store

Na Home temos um outro atributo que é o favorites. Vamos passar para a store a lista de favoritos e vamos adiciona-lo na computed

```js
state {
  pokemons: [
    ...
  ],
  favorites: []
}
```

```js
computed: {
  pokemons: () {
    ...
  },
  favorites: () {
    return this.$store.state.favorites
  }
}
```

Quando mudamos a lista de favoritos para a store o adicionar e remover favoritos para de funcionar, isto porque, não podemos alterar o valor de um atributo da store diretamente e devemos fazer isso utilizando as `mutations`

## Mutations

Mutations é o bloco que nos permite alterar o valor dos atributos dentro da state.
Uma mutation recebe como parametro um state e o payload que representa o valor a ser atribuido.  
Vamos adicionar as mutations de adicionar e remover favoritos

```js
mutations: {
  ADD_FAVORITE(state, payload) {
    state.favorites.push(payload)
  },
  REMOVE_FAVORITE(state, payload) {
    state.favorites.splice(payload, 1)
  }
}
```

Para chamar uma mutation podemos utilizar o `commit`.  
Na Home os métodos de adicionar e remover favoritos ficam assim

```js
methods: {
  addFav(id) {
    const fav = this.pokemons.find((x) => x.id === id)
    this.$store.commit("ADD_FAVORITE", fav)
  },
  removeFav(id) {
    const idx = this.favorites.map(x => x.id).indexOf(id)
    this.$store.commit("REMOVE_FAVORITE", idx)
  }
}
```

Agora podemos adicionar e remover na lista de favoritos da store.  
Vamos atualizar nossa view `Favorites` com as novas mudanças.
Em Favorites temos uma lista de favoritos e temos um metodo para remover um favorito. Podemos remover a lista de favoritos e com a computed pegamos a lista da store e podemos utilizar a mesma mutation da Home para remover um favorito.

```js
computed: {
  favorites() {
    return this.$store.state.favorites
  }
}
```

```js
methods: {
  removeFav(id) {
    const idx = this.favorites.map(x => x.id).indexOf(id)
    this.$store.commit("REMOVE_FAVORITE", idx)
  }
}
```

Com as novas mudanças, quando adicionamos um pokemon como favorito pela página inicial podemos ve-lo na página de favoritos tambem, isto porque, a lista de favoritos está centralizada dentro da store então, desta forma, todas alterações são acessadas globalmente.  
Agora tudo parece certo, porem, quando adicionamos um favorito, navegamos para a lista de favoritos e voltamos para a página inicial, perdemos a referência dos favoritos e com isso corremos o risco de adicionar um valor duplicado dentro da lista. A lista de favoritos se mantem entre as páginas, entretanto, é necessario implementar um sistema que reconhece os favoritos já existentes e então marca-os automaticamente com favoritos logo quando acessamos a página inicial.

## Getters

Os getters, mais do que um meio de acessar os states, é uma forma de filtrarmos informações.  
Seguindo nosso exemplo, precisamos de um sistema que diga se um pókemon é favorito ou não. A lista de favoritos contém todos os pókemons favoritos, então, sabendo disso, podemos criar um método getter que recebe um objeto e retorna verdadeiro ou falso para caso seja um favorito.  
Um getter recebe por definição um state mas podemos informar mais parametros utilizando arrow functions. Vamos criar um getter para verificar, pelo id, se um pókemon é ou não favorito

```js
getters {
  isFavorite: (state) => (id) => {
    return state.favorites.some(x => x.id === id)
  }
}
```

No componente `PokeCard` podemos remover o atributo `fav`, criamos um novo metodo que recebe um id como parametro e faz a chamada para o getter e substituimos no if da linha 19 pelo metodo criado

```js
methods: {
  isFavorite(id) {
    return this.$store.getters.isFavorite(id)
  }
}
```

```js
<v-chip v-if="isFavorite(data.id)"
  color="#ffff"
  style="cursor: pointer"
>
  ...
</v-chip>
```

E então atualizamos o método update para utilizar o `isFavorite`

```js
methods: {
  update(id) {
    if (!this.isFavorite(id)) {
      this.$emit('addFav', id)
      return
    }

    this.$emit('removeFav', id)
  }
}
```

Se um pókemon não é favorito, adicionamos-o como favorito se não removemos

Uma outra aplicação para o getter seria atualizar a quantidade de favoritos na toolbar. Para isso podemos criar um método getter que retorna o tamanho da lista de favoritos

```js
getters {
  qtyOfFavorites(state) => {
    return state.favorites.length
  }
}
```

Para exibir o resultado basta criar a computed para pegar o valor e então referencia-lo no template

```js
export default {
  name: "AppBar",
  computed: {
    qtyOfFavorites() {
      return this.$store.getters.qtyOfFavorites
    }
  }
}
```

```js
<v-chip
  class="ma-0"
  color="#A788A5"
  text-color="white"
  @click="$router.push({ name: 'Favorites' })"
>
  {{ qtyOfFavorites }}
  <v-icon right>
    mdi-heart
  </v-icon>
</v-chip>
```

Finalmente temos um sistema mais estável. Podemos atráves da página inicial adicionar e remover favoritos, na toolbar temos um contagem de favoritos, na página de favoritos vemos somente aqueles que foram previamente adicionados, podemos remover um favorito e então quando voltamos para a página inicial, aqueles que foram removidos passam a não estar mais em destaque. Tudo isso acontece por conta dos dados que estão centralizados na store com o vuex.  

Atualmente nossa lista inicial de pókemons é estatica, isto é, temos 9 pókemons fixos e para mudar isto devemos adiciona-los manualmente. Antes de finalizar o codelab podemos utilizar de uma API de terceiros para popular nossa lista com a quantidade de pókemons que quisermos. Para isso iremos utilizar a [PokeAPI](https://pokeapi.co/)

## Actions

Actions são similares as mutations porém elas não acessam um state diretamente e permitem operações assincronas. Por definição uma mutation não deve ser "commitada" atráves de um componente e sim devemos fazer um dispatch para uma action que então faz o commit em uma mutation que por fim realiza a alteração do state.

![actions-flow](https://user-images.githubusercontent.com/43768274/192058350-0f918e42-41e0-4093-b549-e136c7f1e81f.png)

Antes de integrar a PokeAPI com nosso serviço vamos ajustar o codigo para seguir as definições do vuex. Os métodos da Home e Favorites serão movidos para as actions e no lugar faremos o `dispatch`.

```js
actions: {
  addFavorite({ commit }, payload) {
    const fav = this.state.pokemons.find((x) => x.id === payload)
    commit("ADD_FAVORITE", fav)
  },
  removeFavorite({ commit }, payload) {
    const idx = this.state.favorites.map(x => x.id).indexOf(payload)
    commit("REMOVE_FAVORITE", idx)
  }
},
```

```js
methods: {
  addFav(id) {
    this.$store.dispatch("addFavorite", id)
  },
  removeFav(id) {
    this.$store.dispatch("removeFavorite", id)
  }
}
```

Agora que tudo está certo podemos criar nossa nova action para popular a lista de pokemons.  
Como nossa lista sera dinamicamente populada, podemos deixa-la como vazia no state.

```js
state: {
  pokemons: []
}
```

Na action receberemos um payload que representa a quantidade de items a serem retornados, faremos a chamada para pegar a quantidade de items que queremos e por fim faremos o commit para a mutation.

```js
mutations: {
  SET_POKEMONS(state, payload) {
    state.pokemons = payload
  }
}
```

```js
actions: {
  async getPokemons({ commit }, payload) {
    const imageUrlofficial = "https://raw.githubusercontent.com/zekinah/zone-pokedex2/master/src/assets/images/pokemon/";

    fetch("https://pokeapi.co/api/v2/pokemon?limit=" + payload)
      .then((res) => {
        return res.json()
      })
      .then((data) => {
        const pokemons = data.results.map(subdata => {
          const id = subdata.url.split("/")[subdata.url.split("/").length - 2]
          return {
            id,
            ...subdata,
            imageUrl: `${imageUrlofficial}${id}.png`
          }
        })
        commit("SET_POKEMONS", pokemons)
      })
  }
}
```

Então por fim, na Home, pelo metodo created, fazemos o dispatch para a action que irá popular nossa lista.

```js
created() {
  this.$store.dispatch("getPokemons", 15)
},
```