# Contribuindo

## Preparando Ambiente

Inicialmente clone o repositório das documentações com o comando a seguir.

```
git clone https://gitlab.com/lg-facens/lg-docs
```

Após clonar o repositório abra-o em uma ide de preferência.

## Adicionando um novo documento

Para adicionar um novo documento, primeiro, crie uma nova pasta para o seu documento na raiz do repositório. A sua pasta deve conter um arquivo chamado `README.md` que é o primeiro arquivo que aparecerá quando seu documento for aberto. O arquivo `README` ira apontar para todos os outros arquivos que estiveram no mesmo local, sendo que, todos os arquivos devem ser no formato markdown `md`.

`Caso haja alguma dúvida em relação as formatações em markdown segue um link sobre: https://www.markdownguide.org/cheat-sheet/`

## Ligando seu documento ao sumário e a página inicial

O sumário contem um link da sua página principal e das seções na barra lateral.  
No arquivo `SUMMARY.md` que está na raiz do repositório adicione um link para todos os arquivos do seu documento seguindo o padrão:

```
* [nome_da_seção] (pasta/README.md)
    * [nome_da_subseção1] (pasta/subseção1.md)
    * [nome_da_subseção2] (pasta/subseção2.md)
    ...
```

No arquivo `README.md` na raiz do repositório crie um link na página inicial para o seu documento seguindo o padrão.

```
{ content-ref url="pasta_documento/" }
[nome_pasta][pasta_documento/]
{ endcontent-ref }

dentro dos colchetes, no começo e no fim deve conter um simbolo de %
```